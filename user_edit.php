<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/menu.js"></script>
    <link type="text/css" rel="stylesheet" href="css/style.css">

    <script src="js/jquery.star.rating.js"></script>
</head>

<body>
    <!-- User_edit -->
    <nav>
        <!-- Nav -->
        <div class="nav-wrapper col s9">

            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <a href="#" class="brand-logo left hide-on-med-and-down hide-on-small-only">Logo</a>
            <a href="#" class="brand-logo hide-on-large-only	">Logo</a>
            <ul class="right hide-on-med-and-down">
                <div class="left">
                    <li><a href="index.php" class="transition">Inicio</a></li>
                    <li><a href="#">Todos los libros</a></li>
                    <li><a href="#">Últimos añadidos</a></li>
                    <li><a href="#">Más Leidos</a></li>
                    <li><a href="#">Mejor votados</a></li>
                    <li><a href="#">Categorías</a></li>
                    <li>
                        <!-- Buscador -->
                        <div class="nav-wrapper">
                            <form>
                                <div class="input-field">
                                    <input id="search" type="search" required>
                                    <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                                    <i class="material-icons">close</i>
                                </div>
                            </form>
                        </div>
                    </li>

                </div>
            </ul>
            <ul class="side-nav" id="mobile-demo">
                <li><a href="index.php" class="transition">Inicio</a></li>
                <li><a href="#">Todos los libros</a></li>
                <li><a href="#">Últimos añadidos</a></li>
                <li><a href="#">Más Leidos</a></li>
                <li><a href="#">Mejor votados</a></li>
                <li><a href="#">Categorías</a></li>
            </ul>
        </div>
    </nav>
    <!-- Cierre Nav -->
    
    <div class="container">

        
        <div class="row">
            <div class="center-align">
                <h3>Editar Usuario</h3>
            </div>
        </div>
        <!-- Editar usuario -->
        <div class="row">
            <form action="#">
                <div class="waves-effect btn-large waves-light tooltipped col s12 l3 grey lighten-1 z-depth-1 file-field input-field" data-tooltip="Añadir foto de perfil">
                    <i class="material-icons">image</i></a>
                    <input type="file">
                </div>
            </form>
            <form class="col s12 l9" action="#">
                <div class="row">
                    <div class="input-field col s12 l5">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Nombre</label>
                    </div>
                    <div class="input-field col s12 l5">
                        <i class="material-icons prefix hide-on-large-only">account_circle</i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Apellidos</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l5">
                        <i class="material-icons prefix">email</i>
                        <input id="email" type="email" class="validate">
                        <label for="email" data-error="wrong" data-success="right">Email</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l5">
                        <i class="material-icons prefix">lock</i>
                        <input id="passwd" type="password" class="validate">
                        <label for="password">Contraseña</label>
                    </div>
                    <div class="input-field col s12 l5">
                        <i class="material-icons prefix">loop</i>
                        <input id="passwd-check" type="password" class="validate">
                        <label for="password">Repetir Contraseña</label>
                    </div>
                </div>
            </form>
        </div>
        <!-- Gustos -->
        <div class="row">
            <div class="center-align">
                <h3>Mis gustos</h3>
            </div>
        </div>
    
        <div class="row">
            <div class="center-align col m4">
                <form action="#">
                    <p>
                        <input type="checkbox" id="test1" />
                        <label for="test1">Categoría</label>
                    </p>
                    <p>
                        <input type="checkbox" id="test2" />
                        <label for="test2">Categoría</label>
                    </p>
                </form>
            </div>
            <div class="center-align col m4">
                <form action="#">
                    <p>
                        <input type="checkbox" id="test1" />
                        <label for="test1">Categoría</label>
                    </p>
                    <p>
                        <input type="checkbox" id="test2" />
                        <label for="test2">Categoría</label>
                    </p>
                </form>
            </div>
            <div class="center-align col m4">
                <form action="#">
                    <p>
                        <input type="checkbox" id="test1" />
                        <label for="test1">Categoría</label>
                    </p>
                    <p>
                        <input type="checkbox" id="test2" />
                        <label for="test2">Categoría</label>
                    </p>
                </form>
            </div>
        </div>
    
        <div class="row center-align">
            <button class="btn waves-effect waves-light" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
          </button>
        </div>
    </div>

</body>
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            © 2014 Copyright Text
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
        </div>
    </div>
</footer>

</html>