DELIMITER //
	CREATE PROCEDURE nuevo_libro_leido(
		IN id_libros INT(11),
        IN id_usuarios INT(11),
        IN ult_pagina INT,
        IN fecha_visto DATE,
        IN calificacion INT(1),
        IN comentario mediumtext
    )
	BEGIN
    INSERT INTO libros_leidos (
		id_libros,
        id_usuarios,
        ult_pagina,
        fecha_visto,
        calificacion,
        comentario
    )
	VALUES(
		id_libros,
        id_usuarios,
        ult_pagina,
        fecha_visto,
        calificacion,
        comentario
    );
    
    END//
DELIMITER ;