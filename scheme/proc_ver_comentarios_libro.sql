DELIMITER //

CREATE PROCEDURE ver_comentarios_libro(
	IN id_libro INT(11)
)
BEGIN

	SELECT libros_leidos.calificacion , libros_leidos.comentario, usuarios.nombre, usuarios.apellido_p , usuarios.apellido_m FROM libros_leidos
		INNER JOIN usuarios ON usuarios.id_usuarios = libros_leidos.id_usuarios
        INNER JOIN libros ON libros.id_libros = libros_leidos.id_libros
	WHERE libros.id_libros = id_libro AND usuarios.activo = 1;
    
END //

DELIMITER ;