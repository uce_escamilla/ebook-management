DELIMITER //
	CREATE PROCEDURE nuevo_usuario(
    IN correo varchar(45), 
    IN nombre varchar(100),
    IN apellido_p varchar(150),
    IN apellido_m varchar(150),
    IN passwd varchar(32),
    IN ico_usuario varchar(1)
    )
	BEGIN
		INSERT INTO usuarios (
			correo,
            nombre,
            apellido_p,
            apellido_m,
            fecha_alta,
            fecha_baja,
            activo,
            passwd,
            ico_usuario
            ) 
        VALUES (
			correo,
			nombre,
			apellido_p,
			apellido_m,
			CURDATE(),
			null,
			1,
			passwd,
            ico_usuario
            );
	END //
DELIMITER ;