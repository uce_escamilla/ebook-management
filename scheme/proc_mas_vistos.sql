DELIMITER //

CREATE PROCEDURE mas_leidos(IN limit_vistos INT)
BEGIN
	select id_libros, count(id_libros) as veces_leido from libros_leidos group by id_libros desc limit limit_vistos;
END //
DELIMITER ;