DELIMITER //

CREATE PROCEDURE actualiza_calificacion_libro(
	IN id_libro INT(11)
)
BEGIN

	DECLARE nueva_calificacion FLOAT(2) DEFAULT 0;

	SELECT round(SUM(libros_leidos.calificacion)/COUNT(libros_leidos.id_libros_leidos),2) INTO nueva_calificacion FROM libros_leidos
		INNER JOIN usuarios ON usuarios.id_usuarios = libros_leidos.id_usuarios
        INNER JOIN libros ON libros.id_libros = libros_leidos.id_libros
	WHERE libros.id_libros = id_libro AND usuarios.activo = 1;
    
    UPDATE libros
	SET libros.calificacion = nueva_calificacion
	WHERE id_libros = id_libro; 
    
END //

DELIMITER ;