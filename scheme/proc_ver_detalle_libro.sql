DELIMITER //

CREATE PROCEDURE ver_detalle_libro(
	IN id_libro INT(11)
)
BEGIN

	SELECT * FROM libros as l
		INNER JOIN c_editorial ON l.id_editorial = c_editorial.id_editorial
	WHERE id_libros = id_libro LIMIT 1;

END //

DELIMITER ;