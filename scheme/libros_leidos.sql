DELIMITER //
	CREATE PROCEDURE libros_leidos(
		IN id_usuario INT(11)
	)
    BEGIN
		select libros.nombre as nombre_libro from libros_leidos 
		inner join usuarios on usuarios.id_usuarios = libros_leidos.id_usuarios
		inner join libros on libros.id_libros = libros_leidos.id_libros
		where
		usuarios.id_usuarios = id_usuario;
    END//
DELIMITER ;




