DELIMITER //
	CREATE PROCEDURE nuevo_categoria_con_libro(
		IN id_categoria INT(11),
        IN id_libros INT(11)
    )
	BEGIN
    INSERT INTO categorias_libros
    (
		id_libros,
        id_categorias
    )
    VALUES
    (
		
        id_libros,
        id_categoria
    );
    END //
DELIMITER ;