DELIMITER //
	CREATE PROCEDURE nuevo_libro(
		IN nombre varchar(100), 
		IN autores varchar(45),
		IN calificacion INT(1),
		IN isbn varchar(45),
		IN resenia mediumtext,
		IN url_portada varchar(255),
		IN url_file varchar(255),
		IN editorial int(11)
    )
	BEGIN
    INSERT INTO libros (
            nombre,
            autores,
            calificacion,
            isbn,
            resenia,
            fecha_creacion,
            url_portada,
            url_file,
            id_editorial,
            num_vistas,
            activo)
	VALUES(
			nombre,
            autores,
            calificacion,
            isbn,
            resenia,
            curdate(),
            url_portada,
            url_file,
            editorial,
            0,
            1
	);
    END //
DELIMITER ;
    