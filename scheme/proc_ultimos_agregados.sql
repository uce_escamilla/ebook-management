DELIMITER //

CREATE PROCEDURE ultimos_agregados(IN num_ultimos INT(1))
BEGIN
select libros.id_libros, libros.nombre from libros 
order by fecha_creacion DESC LIMIT num_ultimos;
END //
DELIMITER ;