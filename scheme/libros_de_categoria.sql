# lista de libros de una categoria

DELIMITER //
CREATE PROCEDURE libros_de_categoria(IN id_categoria INT(11))
BEGIN
select libros.id_libros as id_libro, libros.nombre as nombre_libro from categorias_libros 
	inner join c_categorias on c_categorias.id_categorias = categorias_libros.id_categorias
	inner join libros on libros.id_libros = categorias_libros.id_libros
where c_categorias.id_categorias = id_categoria;

END //
DELIMITER ;