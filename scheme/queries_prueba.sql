CALL nuevo_usuario(
'johana.jazmin@hotmail.com',
'Johana Jazmin',
'Solis',
'Arroyo',
'003805592b0de4f05f94972879428c01',
'1'
);

CALL nuevo_usuario(
'ulises.escamilla@hotmail.com',
'Ulises',
'De la cruz',
'Escamilla',
'd1e359eead595db54eecbaed742fdc54',
'2'
);

call nuevo_editorial('salamandra');

call nuevo_libro(
    'HARRY POTTER Y LA PIEDRA FILOSOFAL',
    'J.K. ROWLING ',
    4,
    '8478887598',
    #'Las tediosas vacaciones de verano en casa de sus tíos todavía no han acabado y Harry se encuentra más inquieto que nunca. Apenas ha tenido noticias de Ron y Hermione, y presiente que algo extraño está sucediendo en Hogwarts. En efecto, cuando por fin comienza otro curso en el famoso colegio de magia y hechicería, sus temores se vuelven realidad. El Ministerio de Magia niega que Voldemort haya regresado y ha iniciado una campaña de desprestigio contra Harry y Dumbledore, para lo cual ha asignado a la horrible profesora Dolores Umbridge la tarea de vigilar todos sus movimientos. Así, pues, además de sentirse solo e incomprendido, Harry sospecha que Voldemort puede adivinar sus pensamientos, e intuye que el temible mago trata de apoderarse de un objeto secreto que le permitiría recuperar su poder destructivo.',
    'Harry Potter se ha quedado huérfano y vive en casa de sus abominables tíos y del insoportable primo Dudley. Harry se siente muy triste y solo, hasta que un buen día recibe una carta que cambiará su vida para siempre. En ella le comunican que ha sido aceptado como alumno en el colegio interno Hogwarts de magia y hechicería. A partir de ese momento, la suerte de Harry da un vuelco espectacular. En esa escuela tan especial aprenderá encantamientos, trucos fabulosos y tácticas de defensa contra las malas artes. Se convertirá en el campeón escolar de Quidditch, especie de fútbol aéreo que se juega montado sobre escobas, y se hará un puñado de buenos amigos, aunque también algunos temibles enemigos. Pero sobre todo conocerá los secretos que le permitirán cumplir con su destino. Pues, aunque no lo parezca a primera vista, Harry no es un chico común y corriente. ¡Es un verdadero mago!',
    'libros/harry_potter_y_la_piedra_filosofal/portada_harry_potter_y_la_piedra_filosofal.png',
    'libros/harry_potter_y_la_piedra_filosofal/harry_potter_y_la_piedra_filosofal.pdf',
    1
);

call nuevo_categoria (
	'magia'
);

# Par categoria, libro
call nuevo_categoria_con_libro(
	3,1
);


call nuevo_libro_leido(
	2, # id_libro
    3, # id_usuario
    10, # ult_pagina
    curdate(), # fecha leido,
    5, 	# calificacion
    'Comentario de Ulises' # comentario
);

 call libros_leidos(1);
 
 call ultimos_agregados(5);
