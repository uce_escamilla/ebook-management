# categorias de un libro

DELIMITER //
CREATE PROCEDURE categorias_del_libro(IN id_libro INT(11))
BEGIN
select c_categorias.id_categorias, c_categorias.nombre as nombre_libro from categorias_libros 
	inner join c_categorias on c_categorias.id_categorias = categorias_libros.id_categorias
	inner join libros on libros.id_libros = categorias_libros.id_libros
where libros.id_libros = id_libro;
END //
DELIMITER ;