$(document).ready(function () {

    //$(".button-collapse").sideNav();
    $('.button-collapse').sideNav({
        menuWidth: 300, // Default is 300
        edge: 'right', // Choose the horizontal origin
        closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
        draggable: true, // Choose whether you can drag to open on touch screens,

    });

    $('.carousel').carousel();

    /**
     * Selector
     */
    $('select').material_select();

    /**
     * Cambiar seleccion
     */
    //$('select').material_select('destroy');

    /**
     * Slider
     */
    $('.slider').slider({
        indicators: false
    });

    /**
     * Rating
     */
    //$('.rating').addRating();

    /**
     * scrollspy
     */
    $('.scrollspy').scrollSpy();

    /* Transicion */
    $("body").css("display", "none");
    
    $("body").fadeIn(2000);
    
    $("a.transition").click(function(event){
        event.preventDefault();
        linkLocation = this.href;
        $("body").fadeOut(1000, redirectPage);

    });
        
    function redirectPage() {
        //window.location = linkLocation;
        window.location.replace(linkLocation);
    }

    if(localStorage.getItem("pdfjs.history")) {
        
        // Borramos
        localStorage.setItem("pdfjs.history","");
    }
});