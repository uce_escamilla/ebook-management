<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/menu.js"></script>
    <link type="text/css" rel="stylesheet" href="css/style.css">

    <script src="js/jquery.star.rating.js"></script>
</head>

<body>
    <nav>
        <!-- Nav -->
        <div class="nav-wrapper col s9">

            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <a href="#" class="brand-logo left hide-on-med-and-down hide-on-small-only">Logo</a>
            <a href="#" class="brand-logo hide-on-large-only	">Logo</a>
            <ul class="right hide-on-med-and-down">
                <div class="left">
                    <li><a href="#">Inicio</a></li>
                    <li><a href="#">Todos los libros</a></li>
                    <li><a href="#">Últimos añadidos</a></li>
                    <li><a href="#">Más Leidos</a></li>
                    <li><a href="#">Mejor votados</a></li>
                    <li><a href="#">Categorías</a></li>
                    <li>
                        <!-- Buscador -->
                        <div class="nav-wrapper">
                            <form>
                                <div class="input-field">
                                    <input id="search" type="search" required>
                                    <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                                    <i class="material-icons">close</i>
                                </div>
                            </form>
                        </div>
                    </li>

                </div>
            </ul>
            <ul class="side-nav" id="mobile-demo">
                <li><a href="#">Inicio</a></li>
                <li><a href="#">Todos los libros</a></li>
                <li><a href="#">Últimos añadidos</a></li>
                <li><a href="#">Más Leidos</a></li>
                <li><a href="#">Mejor votados</a></li>
                <li><a href="#">Categorías</a></li>
            </ul>
        </div>
    </nav>
    <!-- Cierre Nav -->

    <div class="row">
        <div class="center-align">
            <h3>Agregar Libro</h3>
        </div>
    </div>
    <!-- Editar libro -->
    <div class="container">

        <div class="row">
            <!-- Insertar archivos -->
            <form action="#">
                <div class="waves-effect btn-large waves-light tooltipped col s12 l6 grey lighten-1 z-depth-1 file-field input-field" data-tooltip="Agregar libro PDF/EPUB">
                    <i class="material-icons">insert_drive_file</i></a>
                    <input type="file">
                </div>
            </form>
            <form action="#">
                <div class="waves-effect btn-large waves-light tooltipped col s12 l6 grey lighten-1 z-depth-1 file-field input-field" data-tooltip="Agregar portada">
                    <i class="material-icons">image</i></a>
                    <input type="file">
                </div>
            </form>
        </div>
        <!-- Cierre insertar archivos -->

        <div class="row">
            <div class="rating left"></div>
        </div>
        <!-- Rating -->

        <div class="row">
            <!-- Conjunto formulario -->
            <form class="col s12 m6">
                <!-- Division izquierda de resenia -->
                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="textarea1" class="materialize-textarea"></textarea>
                        <label for="textarea1">Reseña</label>
                    </div>
                </div>
            </form>
            <!-- Cierre division izquierda de resenia -->
            <div class="col s12 m6">
                <!-- Division derecha de formulario -->
                <div class="row">
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="titulo" type="text" class="validate">
                                <label for="titulo">Título</label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="autor" type="text" class="validate">
                                <label for="autor">Autores</label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="editorial" type="text" class="validate">
                                <label for="editorial">Editorial</label>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    <form class="col s12">
                        <div class="row">
                            <div class="input-field col s12">
                                <input id="isbn" type="text" class="validate">
                                <label for="isbn">ISBN</label>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- Cierre division derecha de formulario -->
        </div>
        <!-- Cierre conjunto formulario -->

        <!-- Categorias del libro -->
        <div class="row">
            <div class="center-align">
                <h3>Categorías</h3>
            </div>
        </div>

        <div class="row">
            <div class="center-align col m4">
                <form action="#">
                    <p>
                        <input type="checkbox" id="test1" />
                        <label for="test1">Categoría</label>
                    </p>
                    <p>
                        <input type="checkbox" id="test2" />
                        <label for="test2">Categoría</label>
                    </p>
                </form>
            </div>
            <div class="center-align col m4">
                <form action="#">
                    <p>
                        <input type="checkbox" id="test1" />
                        <label for="test1">Categoría</label>
                    </p>
                    <p>
                        <input type="checkbox" id="test2" />
                        <label for="test2">Categoría</label>
                    </p>
                </form>
            </div>
            <div class="center-align col m4">
                <form action="#">
                    <p>
                        <input type="checkbox" id="test1" />
                        <label for="test1">Categoría</label>
                    </p>
                    <p>
                        <input type="checkbox" id="test2" />
                        <label for="test2">Categoría</label>
                    </p>
                </form>
            </div>
        </div>

        <div class="row center-align">
            <button class="btn waves-effect waves-light" type="submit" name="action">Agregar
            <i class="material-icons right">note_add</i>
          </button>
        </div>

    </div>
    <!-- Cierre container -->

</body>
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            © 2014 Copyright Text
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
        </div>
    </div>
</footer>

</html>