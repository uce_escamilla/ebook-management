<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Libros_model extends CI_Model {
    
        public function __construct()
        {
                parent::__construct();
                $this->load->database();
        }

        /**
         * Checa si el id del libro existe en la BD
         */
        public function existe_id( $id )
        {
                $this->db->where('id_libros=', $id);
                $query = $this->db->get('libros');
                if( $query->num_rows() >= 1 )
                {
                        return TRUE;
                }
                else
                {
                        return FALSE;
                }
        }

        /**
         * Relaciona la tabla con el c_editorial para regresar todos los libros
         */
        public function get_todos_libros()
        {
                $this->db->from('libros');
                $this->db->where('activo=', 1);
                $this->db->join('c_editorial', 'libros.id_editorial = c_editorial.id_editorial', 'inner');
                $libros = $this->db->get();
                

                if ( !$libros->result_array() )
                {
                        return $this->db->error();
                }
                else
                {
                        $libros_arr = $libros->result_array();

                        return $libros_arr;
                }
                
                
        }

        /**
         * Ultimos libros agregados, se indica cantidad a regresar
         */
        public function get_ultimos( $num )
        {
                $this->db->from('libros');
                $this->db->where('activo=', 1);
                $this->db->join('c_editorial', 'libros.id_editorial = c_editorial.id_editorial', 'inner');
                $this->db->order_by('fecha_creacion','DESC');
                $this->db->limit($num);
                $libros = $this->db->get();

                if ( !$libros->result_array() )
                {
                        return $this->db->error();
                }
                else
                {
                        $libros_arr = $libros->result_array();
                        return $libros_arr;
                }
        }

        /**
         * Catalogo de editoriales
         */
        public function get_nombres_editorial()
        {
                // Del catalogo c_editorial, ordenar ascendentemente
                $this->db->from('c_editorial');
                $this->db->order_by('nombre_edit','ASC');
                $editoriales = $this->db->get();

                // Retorna error o el array
                if ( !$editoriales->result_array() )
                {
                        return $this->db->error();
                }
                else
                {
                        $editoriales_arr = $editoriales->result_array();
                        return $editoriales_arr;
                }
        }
        /**
         * Catalogo de categorias
         */
        public function get_nombres_categorias()
        {
                // Del catalogo c_categorias, ordenar ascendentemente
                $this->db->from('c_categorias');
                $this->db->order_by('nombre','ASC');
                $categorias = $this->db->get();

                // Retorna error o el array
                if ( !$categorias->result_array() )
                {
                        return $this->db->error();
                }
                else
                {
                        $categorias_arr = $categorias->result_array();
                        return $categorias_arr;
                }
        }

        public function set_nuevo_libro( $datos )
        {        
                $this->db->insert('libros', $datos);
        }

        public function get_portada_url( $id )
        {
                $this->db->from('libros');
                $this->db->where('id_libros=',$id);
                $this->db->limit(1);
                $libros = $this->db->get();

                if ( !$libros->row() )
                {
                        return $this->db->error();
                }
                else
                {
                        $row = $libros->row();
                        return $row->url_portada;
                }
        }

        public function get_libro_url( $id )
        {
                $this->db->from('libros');
                $this->db->where('id_libros=',$id);
                $this->db->limit(1);
                $libros = $this->db->get();

                if ( !$libros->row() )
                {
                        return $this->db->error();
                }
                else
                {
                        $row = $libros->row();
                        return $row->url_file;
                }
        }


    
    }
?>