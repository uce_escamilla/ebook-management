<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_Model 
{
    
    public function __construct()
    {
            parent::__construct();
            $this->load->database();
    }

    /**
     * Corrobora si el libro ha sido leido por el usuario
     */
    public function libro_leido( $id_usuario, $id_libro )
    {
        $this->db->from('libros_leidos');
        $this->db->where('libros.id_libros=', $id_libro);
        $this->db->where('usuarios.id_usuarios=', $id_usuario);
        $this->db->join('usuarios', 'usuarios.id_usuarios = libros_leidos.id_usuarios', 'inner');
        $this->db->join('libros', 'libros.id_libros = libros_leidos.id_libros', 'inner');
        $query = $this->db->get();

        if( $query->num_rows() >= 1 )
        {
                return TRUE;
        }
        else
        {
                return FALSE;
        }
    }

    /**
     * Se agrega a la BD como libro leido
     */
    public function nuevo_libro_leido( $id_usuario, $id_libro )
    {
        $data = array(
            'id_libros'     => $id_libro,
            'id_usuarios'   => $id_usuario,
            'ult_pagina'    => 1,
            'fecha_visto'   => date("Y-m-d"),
            'calificacion'  => '',
            'comentario'    => ''
        );

        if ( ! $this->db->insert('libros_leidos', $data) )
        {
            $error = $this->db->error(); 
        }
        else
        {
            return TRUE;
        }

        
    }

    /**
     * Datos pagina de libro leido
     */
    public function get_pagina_libro( $id_usuario, $id_libro )
    {
        $this->db->select('ult_pagina');
        $this->db->from('libros_leidos');
        $this->db->where('libros.id_libros=', $id_libro);
        $this->db->where('usuarios.id_usuarios=', $id_usuario);
        $this->db->join('usuarios', 'usuarios.id_usuarios = libros_leidos.id_usuarios', 'inner');
        $this->db->join('libros', 'libros.id_libros = libros_leidos.id_libros', 'inner');
        $query = $this->db->get();

        if ( ! $query->result() )
        {
            $error = $this->db->error(); 
        }
        else
        {
            $resultado = $query->result();

            return $resultado['0'];
        }
        
    }

    /**
     * Actualiza ultima pagina leida de un libro
     */
    public function set_ult_pag( $id_usuario, $id_libro, $pagina_libro )
    {
        $this->db->set('ult_pagina', $pagina_libro);
        $this->db->where('id_libros', $id_libro);
        $this->db->where('id_usuarios', $id_usuario);
        $this->db->update('libros_leidos');
    }

    /**
     * Obtiene arr con los ultimos 10 libros leidos de usr con portada
     */
    public function get_ult_libros( $id_usuario )
    {
        $this->db->select( array('libros.id_libros','libros.url_portada'));
        $this->db->from('libros_leidos');
        $this->db->order_by('fecha_visto', 'DESC');
        $this->db->where('usuarios.id_usuarios=', $id_usuario);
        $this->db->where('libros.activo=', 1);
        $this->db->limit(10);
        $this->db->join('usuarios', 'usuarios.id_usuarios = libros_leidos.id_usuarios', 'inner');
        $this->db->join('libros', 'libros.id_libros = libros_leidos.id_libros', 'inner');

        $query = $this->db->get();
        
        if ( $query->num_rows() >= 1 )
        {
            return $query->result_array();
        }
        else
        {
            return 0; 
        }
    }
   
}