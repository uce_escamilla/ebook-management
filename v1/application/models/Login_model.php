<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {
    
    public function __construct()
    {
            parent::__construct();
            // $this->load->helper(array('form', 'url'));
            // $this->load->library('form_validation');

            $this->load->database();
    }

    public function login_usuario($correo, $contrasenia)
    {
        $this->db->where('correo',$correo);
        $this->db->where('passwd',$contrasenia);
        $query = $this->db->get('usuarios');
        if($query->num_rows() == 1)
        {
            return $query->row();
        }
        else
        {
            //$this->session->set_flashdata('usuario_incorrecto','Los datos introducidos son incorrectos');
            $this->load->view('login',$data = array(
                'error_message' => 'Usuario o contraseña incorrectos.'
            ));
        }
    }
}