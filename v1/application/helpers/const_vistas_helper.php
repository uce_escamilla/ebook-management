<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Constantes para las vistas de cada libro
class Const_Vistas
{
    const LIBROS_POR_FILA           = 4;
    const NUM_VISTAS                = 2;
    const MAX_VISTA_TEXTO_GRID      = 23;
    const VISTA_GRID                = 1;
    const VISTA_LISTA               = 2;
    const MAX_RESENIA_GRID          = 25;
    const MAX_RESENIA_LISTA         = 200;

    const NUM_COL_CHECKBOX          = 3;
}