<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function genera_path( $path )
{
    if(mkdir( $path , 0777, true))
    {
        return TRUE;
    }
    else
    {
        $data['error'] = "Error al generar el directorio";
        
        return FALSE;
    }
    
}

