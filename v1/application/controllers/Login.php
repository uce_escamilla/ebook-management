<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Login extends CI_Controller {

 
        public function __construct()
        {
            parent::__construct();
            $this->load->library(array('session','form_validation'));
            $this->load->helper(array('url','form'));
            //$this->load->database('default');
            $this->load->model('Login_model');
        }

        public function index()
        {
            $this->load->view('login',$data = array(
                'error_message' => ' '
                ));
        }

        public function entrar()
        {
            
            $this->form_validation->set_rules('correo_login', 'Correo', 'trim|required|valid_email');
            $this->form_validation->set_rules('passwd_login', 'Password', 'trim|required');

            // Ejecuta las rutinas de validacion
            if ( $this->form_validation->run() == FALSE ) 
            {
                $this->load->view('login',$data = array(
                    'error_message' => ' '
                ));
            } 
            else 
            {
                $correo = $this->input->post('correo_login');
                $passwd = $this->input->post('passwd_login');
                $result = $this->Login_model->login_usuario($correo, md5($passwd));
                if($result == TRUE)
                {
                    //var_dump($result);
                    $sess_array = array(
                        //'username' => $this->input->post('username')
                        'id_usuarios' => $result->id_usuarios,
                        'correo' => $result->correo,
                        'nombre' => $result->nombre,
                        'apellido_paterno' => $result->apellido_p,
                        'apellido_materno' => $result->apellido_m
                    );
            
                    // Add user data in session
                    $this->session->set_userdata('logged_in', $sess_array);
                    //$result = $this->Login_model->info_usuario($sess_array);
                    if($result != false)
                    {
                        $data = array(
                        'id_usuarios' => $result->id_usuarios,
                        'correo' => $result->correo,
                        'nombre' => $result->nombre,
                        'apellido_paterno' => $result->apellido_p,
                        'apellido_materno' => $result->apellido_m
                        );
                        redirect('v1/'.'Libros/index', $data);
                    }
                }
                else
                {
                    $data = array(
                    'error_message' => 'Usuario o Contraseña Incorrecta'
                    );
                    $this->load->view('login', $data);
                }
            }
        }


        public function cerrar_sesion()
        {
            $this->session->sess_destroy();
            redirect('v1/'.'Login');
        }

}