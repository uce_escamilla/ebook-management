<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Usuarios extends CI_Controller {

 
        public function __construct()
        {
            parent::__construct();
            $this->load->library(array('session','form_validation'));
            $this->load->library('form_validation');
            $this->load->helper('url','form','cookie');
            $this->load->database();
            $this->load->model('Libros_model');
            $this->load->model('Usuarios_model');

            if(!$this->session->userdata('logged_in'))
            {      
                redirect('v1/'.'login');
            }
        }

        public function editar()
        {  
            $this->load->view('editar_perfil');
        }

        /**
         * Redirige al libro segun el ID del mismo
         */

        public function leer_libro( $id_libro )
        {
            // Si el id del libro esta seteado
            if ( isset( $id_libro ) )
            {
                // Corroborar si el id existe en la BD
                if ( $this->Libros_model->existe_id( $id_libro ) )
                {
                    // Rescatamos id usuario de la sesion
                    $id_usuario = $this->session->logged_in['id_usuarios'];

                    // Corroboramos si ya se ha leido el libro
                    if ( $this->Usuarios_model->libro_leido( $id_usuario , $id_libro ) )
                    {
                        
                        // Obtenemos los ultimos datos del libro leido por el usuario
                        $ultima_pagina_leida = $this->Usuarios_model->get_pagina_libro( $id_usuario , $id_libro );

                        $ultima_pagina_leida = $ultima_pagina_leida->ult_pagina;
                    }
                    else
                    {
                        // Agregamos a libros leidos
                        $this->Usuarios_model->nuevo_libro_leido( $id_usuario , $id_libro );
                        $ultima_pagina_leida = 1;
                    }

                    $cookie_expiracion = '86500';

                    $cookie = array(
                        'name'   => 'libro_actual',
                        'value'  => $id_libro,
                        'expire' => $cookie_expiracion
                    );
                    
                    
                    $cookie_pagina = array(
                        'name'   => 'ultima_pagina',
                        'value'  => $ultima_pagina_leida,
                        'expire' => $cookie_expiracion
                    );

                    $this->input->set_cookie($cookie);
                    $this->input->set_cookie($cookie_pagina);

                    $url_libro = $this->Libros_model->get_libro_url( $id_libro );

                    $url_pdf = base_url('v1/'.$this->Libros_model->get_libro_url( $id_libro ));

                    $viewer_pdf_url = base_url('web/viewer.html?file=');

                    $page_url = '#page='.$ultima_pagina_leida;

                    redirect( $viewer_pdf_url.$url_pdf.$page_url );
                }
                else
                {
                    redirect('v1/'.'Libros/todos');
                }

            }
            else
            {
                redirect('v1/'.'Libros/todos');
            }
        }

    }