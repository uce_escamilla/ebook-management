<?php
  defined('BASEPATH') OR exit('No direct script access allowed');

  class Libros extends CI_Controller {

 
        public function __construct()
        {
            parent::__construct();
            $this->load->library('form_validation');
            $this->load->library(array('session','form_validation'));
            $this->load->helper('url','form','cookie');
            $this->load->database();
            $this->load->model('Libros_model');

            $this->load->helper('archivos');
            

            if(!$this->session->userdata('logged_in'))
            {      
                redirect('v1/'.'login');
            }
        }

        public function index()
        {
            $this->load->view('index');  
        }

        public function inicio()
        {
            $this->load->model('Usuarios_model');
            $datos_usuario = $this->session->userdata('logged_in');
            $id_usuario = $datos_usuario['id_usuarios'];
            $data['ultimos_libros'] = $this->Usuarios_model->get_ult_libros($id_usuario);
            $this->load->view('index', $data);
        }

        /**
         * Todos los libros por vista en grid y list
         */
        public function todos( $vista )
        {

            // Clase y metodo para formar el URL a redirigir
            $metodo = $this->router->fetch_method();
            $clase  = $this->router->fetch_class();
            

            // Validar parametro de vista
            if( $vista <= Const_Vistas::NUM_VISTAS && $vista > 0)
            {
                $data['vista'] = $vista;
            }
            else // Redireccion en caso de recibir otro parametro al num de vistas
            {
                $url_vista_grid = base_url();
                $url_vista_grid = $url_vista_grid.'v1/'.$clase.'/'.$metodo.'/1';
                header( 'Location: '.$url_vista_grid );
            }

            $data['libros'] = $this->Libros_model->get_todos_libros();            
            $this->load->view('todos', $data);


        }

        /**
         * Ultimos libros agregados por todos
         */
        public function ultimos_agregados()
        {

        }

        /**
         * Subir libro
         */
        public function subir_libro()
        {
            
            // Valores de restricciones para los archivos
            $config['allowed_types']        = 'pdf|epub|png|jpg';
            $config['max_size']             = 2048;
            $config['max_width']            = 1080;
            $config['max_height']           = 1080;
            $config['remove_spaces']        = TRUE;


            // Preparar el nombre del directorio a generar
            $nombre_libro =  $this->input->post('titulo_libro') ;
            $nombre_libro = str_replace(' ', '_', $nombre_libro);
         
            $path_libro = $nombre_libro;
            // Nombre del libro
            $config['file_name'] = $path_libro;

            // Crear directorio/path
            if( genera_path( './src/libros/'.$path_libro ) )
            {
                $config['upload_path'] = './src/libros/'.$path_libro;
                $folder_libro = 'src/libros/'.$path_libro;
            }

            
            // Generamos la configuracion
            $this->load->library('upload', $config);
            
            // Carga de libro
            if ( ! $this->upload->do_upload('subir_libro') )
            {
                // Errores
                $this->form_validation->set_error_delimiters('<p class="error">', '</p>');
                // Cargar c_editorial
                $data['editoriales'] = $this->Libros_model->get_nombres_editorial();                    
                // Cargar c_categorias
                $data['categorias'] = $this->Libros_model->get_nombres_categorias();  
                $data['error'] = $this->upload->display_errors();

                $this->load->view('nuevo_libro', $data);
            }
            else
            {
                // Carga de libro exitosa
                
                // Preparamos los datos de la portada

                // Datos del archivo a mostrar
                $data = array('upload_data' => $this->upload->data());

                // Path Libro en BD con ruta relativa
                $path_libro = $folder_libro.'/'.$this->upload->data('file_name');

                // Nombre de la portada y path
                $path_portada = 'portada_'.$nombre_libro;
                $config['file_name'] = $path_portada;
                
                // Generamos la configuracion
                $this->upload->initialize($config);
                
                
                // Caso exitoso al subir portada
                if ( ! $this->upload->do_upload('subir_portada') )
                {     
                    // Errores
                    $this->form_validation->set_error_delimiters('<p class="error">', '</p>');  
                    $data['error'] = $this->upload->display_errors();

                    $path_portada = $folder_libro.'/';
                }
                else
                {
                    $data = array('upload_data' => $this->upload->data());
                    // Path Libro en BD con ruta relativa
                    $path_portada = $folder_libro.'/'.$this->upload->data('file_name');
                }
                
                
                // Datos del libro
                $datos_libro['nombre']         = $this->input->post('titulo_libro') ;
                $datos_libro['autores']        = $this->input->post('autor_libro');
                $datos_libro['calificacion']   = 0.0;
                $datos_libro['isbn']           = $this->input->post('isbn_libro');
                $datos_libro['resenia']        = $this->input->post('resenia_libro');
                $datos_libro['fecha_creacion'] = date('Y-m-d');
                $datos_libro['url_portada']    = $path_portada;
                $datos_libro['url_file']       = $path_libro; 
                $datos_libro['id_editorial']   = intval( $this->input->post('editorial_libro') );
                $datos_libro['num_vistas']     = 0;
                $datos_libro['activo']         = 1;
                

                $this->Libros_model->set_nuevo_libro( $datos_libro );

                $this->load->view('success', $data);
            }
        }

        /**
         * Nuevo libro
         */

        public function nuevo()
        {
            // Cargar c_editorial
            $data['editoriales'] = $this->Libros_model->get_nombres_editorial();                    
            // Cargar c_categorias
            $data['categorias'] = $this->Libros_model->get_nombres_categorias();                    

            // Error en blanco
            $data['error'] = ' ';


            $data['upload_data'] = ' ';
            $this->load->view('nuevo_libro', $data);
        }

        /**
         * Sync pagina de libro del cache generado por PDF JS
         */
        public function sync()
        {
            $resultado = $_POST['datos_libro'] ; 
            
            $libro_datos = $resultado['files']['0'];

            $this->load->model('Usuarios_model');

            // Corroborar los datos del libro en las cookies
            if(isset( $_COOKIE['libro_actual'] ))
            {
                $id_libro = $_COOKIE['libro_actual'];
                if(isset( $_COOKIE['ultima_pagina'] ))
                {
                    $ultima_pagina = $_COOKIE['ultima_pagina'];

                    // Usuario de la sesion en el servidor
                    $id_usuario = $this->session->logged_in['id_usuarios'];

                    // Actualizamos cookies de ultima pagina
                    $cookie_expiracion = '86500';
                    
                    $cookie_pagina = array(
                        'name'   => 'ultima_pagina',
                        'value'  => $libro_datos['page'],
                        'expire' => $cookie_expiracion
                    );

                    $this->input->set_cookie($cookie_pagina);

                    $this->Usuarios_model->set_ult_pag( $id_usuario, $id_libro, $libro_datos['page'] );
                    echo "pag actualizada: ".$libro_datos['page'];
                }

            }

        } 

    }