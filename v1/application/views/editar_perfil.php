<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('template/header.php'); ?>
<?php $this->load->view('template/nav.php'); ?>
   
    <div class="container">

        
        <div class="row">
            <div class="center-align">
                <h3>Editar Usuario</h3>
            </div>
        </div>
        <!-- Editar usuario -->
        <div class="row">
            <form action="#">
                <div class="waves-effect btn-large waves-light tooltipped col s12 l3 grey lighten-1 z-depth-1 file-field input-field" data-tooltip="Añadir foto de perfil">
                    <i class="material-icons">image</i></a>
                    <input type="file">
                </div>
            </form>
            <form class="col s12 l9" action="#">
                <div class="row">
                    <div class="input-field col s12 l5">
                        <i class="material-icons prefix">account_circle</i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Nombre</label>
                    </div>
                    <div class="input-field col s12 l5">
                        <i class="material-icons prefix hide-on-large-only">account_circle</i>
                        <input id="icon_prefix" type="text" class="validate">
                        <label for="icon_prefix">Apellidos</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l5">
                        <i class="material-icons prefix">email</i>
                        <input id="email" type="email" class="validate">
                        <label for="email" data-error="wrong" data-success="right">Email</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12 l5">
                        <i class="material-icons prefix">lock</i>
                        <input id="passwd" type="password" class="validate">
                        <label for="password">Contraseña</label>
                    </div>
                    <div class="input-field col s12 l5">
                        <i class="material-icons prefix">loop</i>
                        <input id="passwd-check" type="password" class="validate">
                        <label for="password">Repetir Contraseña</label>
                    </div>
                </div>
            </form>
        </div>
        <!-- Gustos -->
        <div class="row">
            <div class="center-align">
                <h3>Mis gustos</h3>
            </div>
        </div>
    
        <div class="row">
            <div class="center-align col m4">
                <form action="#">
                    <p>
                        <input type="checkbox" id="test1" />
                        <label for="test1">Categoría</label>
                    </p>
                    <p>
                        <input type="checkbox" id="test2" />
                        <label for="test2">Categoría</label>
                    </p>
                </form>
            </div>
            <div class="center-align col m4">
                <form action="#">
                    <p>
                        <input type="checkbox" id="test1" />
                        <label for="test1">Categoría</label>
                    </p>
                    <p>
                        <input type="checkbox" id="test2" />
                        <label for="test2">Categoría</label>
                    </p>
                </form>
            </div>
            <div class="center-align col m4">
                <form action="#">
                    <p>
                        <input type="checkbox" id="test1" />
                        <label for="test1">Categoría</label>
                    </p>
                    <p>
                        <input type="checkbox" id="test2" />
                        <label for="test2">Categoría</label>
                    </p>
                </form>
            </div>
        </div>
    
        <div class="row center-align">
            <button class="btn waves-effect waves-light" type="submit" name="action">Submit
            <i class="material-icons right">send</i>
          </button>
        </div>
    </div>

<?php $this->load->view('template/footer.php'); ?>
