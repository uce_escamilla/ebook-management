<nav>
        <!-- Nav -->
        <div class="nav-wrapper col s9">
            <div class="container valign-wrapper"> 
                <div class="center-align">
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <a href="<?php echo site_url('v1/Usuarios/editar'); ?>" class="brand-logo center-align hide-on-med-and-down hide-on-small-only"><img width="30" src="<?php echo site_url('img/1.png'); ?>"></a>
                <a href="<?php echo site_url('v1/Usuarios/editar'); ?>" class="brand-logo hide-on-large-only"><img width="30" src="<?php echo site_url('img/1.png'); ?>"></a>
                </div>
            </div>
            <ul class="right hide-on-med-and-down">
                <div class="left">
                    <li><a href="<?php echo site_url('v1/Libros/inicio'); ?>">Inicio</a></li>
                    <li><a href="<?php echo site_url('v1/Libros/todos'); ?>">Todos los libros</a></li>
                    <li><a href="#">Últimos añadidos</a></li>
                    <li><a href="#">Más Leidos</a></li>
                    <li><a href="#">Mejor votados</a></li>
                    <li><a href="#">Categorías</a></li>
                    <li>
                        <!-- Buscador -->
                        <div class="nav-wrapper">
                            <form>
                                <div class="input-field">
                                    <input id="search" type="search" required>
                                    <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                                    <i class="material-icons">close</i>
                                </div>
                            </form>
                        </div>
                    </li>
                    <li><a class="waves-effect waves-light btn" href="<?php echo site_url('v1/Login/cerrar_sesion'); ?>">Salir</a></li>

                </div>
            </ul>
            <ul class="side-nav" id="mobile-demo">
                <li><a href="<?php echo site_url('v1/Libros/inicio'); ?>">Inicio</a></li>
                <li><a href="<?php echo site_url('v1/Libros/todos'); ?>">Todos los libros</a></li>
                <li><a href="#">Últimos añadidos</a></li>
                <li><a href="#">Más Leidos</a></li>
                <li><a href="#">Mejor votados</a></li>
                <li><a href="#">Categorías</a></li>
                <li><a href="<?php echo site_url('v1/Login/cerrar_sesion'); ?>">Salir</a></li>
            </ul>
        </div>
        
    </nav>
    <!-- Cierre Nav -->