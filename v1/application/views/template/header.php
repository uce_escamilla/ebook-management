
<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('css/materialize.min.css'); ?>" media="screen,projection" />

    <script type="text/javascript" src="<?php echo base_url('js/menu.js'); ?>"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script type="text/javascript" src="<?php echo base_url('js/materialize.min.js'); ?>"></script>
    <link type="text/css" rel="stylesheet" href="<?php echo base_url('css/style.css'); ?>">
</head>
<body>
