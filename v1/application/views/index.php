<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<?php $this->load->view('template/header.php'); ?>
<?php $this->load->view('template/nav.php'); ?>
 <!-- Botones -->
 <div class="center-align">
        <h3>Opciones</h3>
    </div>
    <div class="container">
        <div class="divider"></div>
    
        <div class="row">
            <a class="waves-effect waves-light btn-large tooltipped col s4 grey lighten-1 z-depth-1" data-position="bottom" data-delay="50"
                data-tooltip="Añadir un libro" href="<?php echo site_url('v1/Libros/nuevo'); ?>">
            <i class="material-icons">add_circle_outline</i></a>
            <a class="waves-effect waves-light btn-large tooltipped col s4 yellow lighten-4 z-depth-1" data-position="bottom" data-delay="50"
                data-tooltip="Sorpréndeme">
            <i class="material-icons">star_border</i></a>
            <a class="waves-effect waves-light btn-large tooltipped col s4 yellow lighten-4 z-depth-1" data-position="bottom" data-delay="50"
                data-tooltip="Estadísticas">
            <i class="material-icons">trending_up</i></a>
        </div>
        <div class="center-align">
            <h3>Continuar Leyendo</h3>
        </div>
        <div class="divider"></div>
        <div class="carousel">
<?php foreach ($ultimos_libros as $libro) :?>
            <a class="carousel-item" href="<?php echo base_url( 'v1/'.'usuarios/leer_libro/').$libro['id_libros']; ?>"><img src="<?php echo base_url( 'v1/'.$libro['url_portada'] ); ?>"></a>
<?php endforeach; ?>
        </div>
    
        <div class="row"></div>
    </div>



<?php $this->load->view('template/footer.php'); ?>
