<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><?php $this->load->view('template/header.php'); ?>
<?php $this->load->view('template/nav.php'); ?>

<h3>Archivo cargado de forma exitosa</h3>
<ul>
<?php foreach ($upload_data as $item => $value):?>
<li><?php echo $item;?>: <?php echo $value;?></li>
<?php endforeach; ?>
</ul>

<?php $this->load->view('template/footer.php'); ?>
