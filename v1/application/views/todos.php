<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><?php $this->load->view('template/header.php'); ?>
<?php $this->load->view('template/nav.php'); ?>



<div class="row">
        <!-- Dropdown Trigger -->
        <a class='dropdown-button hide-on-small-only btn right col l3 offset-l9 col s12' href='#' data-activates='dropdown1'>Vista</a>

        <!-- Dropdown Structure -->
        <ul id='dropdown1' class='dropdown-content right col l3 offset-l9 col s12'>
            <li><a href="1"><i class="material-icons">view_module</i>Grid</a></li>
            <li><a href="2"><i class="material-icons">view_stream</i>Lista</a></li>
        </ul>
</div>

<?php $count=1; foreach ($libros as $libro ) : ?>
<?php if ($count == 1): ?>
    <div class="row"> <!-- Inicio Row -->
<?php endif; ?>
        
        <div class="libro <?php if ($vista == 1 ): echo "col s12 l3"; else: echo "s12"; endif; ?> ">
            <div class="row"></div>
            <div class="slider">
                <ul class="slides">
                    <li> <a target="_blank" href="<?php echo base_url( 'v1/'.'usuarios/leer_libro/').$libro['id_libros']; ?>">
                        <img src="<?php echo base_url( 'v1/'.$libro['url_portada'] ); ?>">
                        <!-- Portada Libro -->
                        <div class="caption center-align">
                            <h3><?php  
                            if ( $vista == Const_Vistas::VISTA_GRID ){ 
                                if(strlen($libro['nombre']) > Const_Vistas::MAX_VISTA_TEXTO_GRID ){ 
                                    echo substr($libro['nombre'], 0, Const_Vistas::MAX_VISTA_TEXTO_GRID )."..."; 
                                }else{ echo $libro['nombre']; } 
                            }else{ echo $libro['nombre']; }?></h3>
                            <h5 class="light grey-text text-lighten-3"><?php 
                                if ( $vista == Const_Vistas::VISTA_GRID ){
                                    echo substr($libro['resenia'], 0, Const_Vistas::MAX_RESENIA_GRID )."...";
                                }else{
                                    echo substr($libro['resenia'], 0, Const_Vistas::MAX_RESENIA_LISTA )."...";
                                }
                                 
                            ?></h5>
                            <div class="chip right"><?php //Realizar modelo ?></div>
                        </div>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="row"></div>
        </div>
<?php if ($count % Const_Vistas::LIBROS_POR_FILA == 0): $count=0;?>
    </div> <!-- Cierre Row -->
<?php endif; $count++;?>



<?php endforeach; // End Foreach ?>

<?php if ($count != 0): ?>
    </div> <!-- Cierre Row -->
<?php endif; ?>



<?php $this->load->view('template/footer.php'); ?>