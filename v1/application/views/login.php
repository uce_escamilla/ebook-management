<?php
defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php $this->load->view('template/header.php'); ?>

<?php
// Datos Formulario Login
$data_usuario = array(
    'name'      =>  'correo_login',
    'id'        =>  'correo', 
    'class'     =>  'validate'
);

$data_passwd = array(
    'name'      =>  'passwd_login',
    'id'        =>  'passwd', 
    'class'     =>  'validate'
);

?>

<div class="container">

    
    <?php echo form_open('v1/'.'Login/entrar'); ?>
    
<div class="isa_error">
<?php echo $error_message; ?>
<?php echo validation_errors(); ?>
</div>

<div class="row">
    <div class="input-field col s12">
        <?php echo form_input($data_usuario); ?>
        <?php echo form_label('Correo');  ?>
    </div>
</div>
<div class="row">
    <div class="input-field col s12">
        <?php echo form_password($data_passwd); ?>
        <?php echo form_label('Contraseña');  ?>
    </div>
</div>

<div class="row center-align">           
<?php echo form_submit('submit', 'Submit', "class='btn waves-effect waves-light'"); ?>
</div>

<?php echo form_close(); // Cierre del formulario?>

</div>



<?php $this->load->view('template/footer.php'); ?>
