<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><?php $this->load->view('template/header.php'); ?>
<?php $this->load->view('template/nav.php'); ?>
<?php 
/**
 * Parametros para el formulario
 */

// Numero de filas por columna para el checkbox de categorias
$cat_por_col = count($categorias) / Const_Vistas::NUM_COL_CHECKBOX;
$restantes_por_col = count($categorias) % Const_Vistas::NUM_COL_CHECKBOX;
// Crear Action de formulario

// Datos Text Area Resenia
$data_resenia = array(
    'name'      =>  'resenia_libro',
    'id'        =>  'resenia', 
    'class'     =>  'materialize-textarea'
    );

// Datos nombre del libro
$data_name = array(
    'name'      => 'titulo_libro',
    'id'        => 'titulo',
    'class'     => 'validate'
    );
// Datos autores
$data_autores = array(
    'name'      => 'autor_libro',
    'id'        => 'autor',
    'class'     => 'validate'
    );
// Datos editorial
$data_editorial = array(
    'id'        => 'editorial',
    'class'     => 'selectpicker',
    'data-live-search'=> 'true'
    );

$options = [];

foreach ($editoriales as $editorial => $values ) {
    $options[ $values['id_editorial'] ] = $values['nombre_edit'];
}



// Datos ISBN
$data_isbn = array(
    'name'      => 'isbn_libro',
    'id'        => 'isbn',
    'class'     => 'validate'
    );
// Datos archivo libro
$data_libro = array(
    'type' => 'file',
    'name' => 'subir_libro'
);
// Datos archivo portada
$data_portada = array(
    'type' => 'file',
    'name' => 'subir_portada'
);


// Cierre de las variables para el formulario
    ?>
<!-- Encabezado -->
<div class="row">
    <div class="center-align">
        <h3>Agregar Libro</h3>
    </div>
</div>

<div class="container">

<?php echo $error; ?>



<?php echo form_open_multipart('v1/'.'libros/subir_libro'); ?>

<div class="row">
    <!-- Insertar archivos -->

    <div class="waves-effect btn-large waves-light tooltipped col s12 l6 grey lighten-1 z-depth-1 file-field input-field" data-tooltip="Agregar libro PDF/EPUB">
        <i class="material-icons">insert_drive_file</i>
        <?php echo form_upload($data_libro); ?>
    </div>


    <div class="waves-effect btn-large waves-light tooltipped col s12 l6 grey lighten-1 z-depth-1 file-field input-field" data-tooltip="Agregar portada">
        <i class="material-icons">image</i>
        <?php echo form_upload($data_portada);?>
    </div>

</div>


<!-- Formulario HTML -->
<div class="row">
<!-- Division izquierda de resenia -->
    <div class="col s12 m6">
        <div class="row">
            <div class="input-field col s12">
                <?php echo form_textarea($data_resenia); ?>
                <?php echo form_label('Reseña'); ?>
            </div>
        </div>
    </div>
<!-- Cierre division izquierda de resenia -->
<div class="col s12 m6">
    <div class="row">
        <div class="input-field col s12">
            <?php echo form_input($data_name); ?>
            <?php echo form_label('Nombre');  ?>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s12">
            <?php echo form_input($data_autores); ?>
            <?php echo form_label('Autores');  ?>
        </div>
    </div>

   
    <div class="row">
        <div class="input-field col s12">
            <?php echo form_dropdown('editorial_libro', $options, '', $data_editorial); ?>
            <?php echo form_label('Editorial');  ?>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            <?php echo form_input($data_isbn); ?>
            <?php echo form_label('ISBN');  ?>
        </div>
    </div>

</div>
</div>

    <!-- Categorias del libro -->
    <div class="row">
        <div class="center-align">
            <h3>Categorías</h3>
        </div>
    </div>
<?php $count=0; foreach ($categorias as $categoria): ?>
<?php if($count == 0): ?>
<div class="row">
<?php endif; $count++;?>
    <div class="center-align col m4">
            <p>
                <input type="checkbox" id="<?php echo $categoria['id_categorias']; ?>" name="<?php echo $categoria['id_categorias']; ?>" />
                <label for="<?php echo $categoria['id_categorias']; ?>"><?php echo ucwords(strtolower($categoria['nombre'])); ?></label>
            </p>
    </div>

<?php if($count == floor($cat_por_col)): $count = 0; ?>
</div>
<?php endif; ?>

<?php endforeach; // Fin Foreach ?>

<?php if( !( $restantes_por_col == 0 ) ): ?>
</div>
<?php endif; ?>

<div class="row center-align">           
<?php echo form_reset('reset', 'Reset', "class='btn waves-effect waves-light'"); ?>
<?php echo form_submit('submit', 'Submit', "class='btn waves-effect waves-light'"); ?>
</div>
    
  

    <?php echo form_close(); // Cierre del formulario?>
</div>
<?php $this->load->view('template/footer.php'); ?>