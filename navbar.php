 <nav>
 <div class="nav-wrapper col s9">

     <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
     <a href="#" class="brand-logo left hide-on-med-and-down hide-on-small-only">Logo</a>
     <a href="#" class="brand-logo hide-on-large-only">Logo</a>
     <ul class="right hide-on-med-and-down">
         <div class="left">
             <li><a href="user_edit.php" class="transition">Inicio</a></li>
             <li><a href="#">Todos los libros</a></li>
             <li><a href="#">Últimos añadidos</a></li>
             <li><a href="#">Más Leidos</a></li>
             <li><a href="#">Mejor votados</a></li>
             <li><a href="#">Categorías</a></li>
             <li>
                 <div class="nav-wrapper">
                     <form>
                         <div class="input-field">
                             <input id="search" type="search" required>
                             <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                             <i class="material-icons">close</i>
                         </div>
                     </form>
                 </div>
             </li>

         </div>
     </ul>
     <ul class="side-nav" id="mobile-demo">
         <li><a href="#">Inicio</a></li>
         <li><a href="#">Todos los libros</a></li>
         <li><a href="#">Últimos añadidos</a></li>
         <li><a href="#">Más Leidos</a></li>
         <li><a href="#">Mejor votados</a></li>
         <li><a href="#">Categorías</a></li>
     </ul>
 </div>
</nav>