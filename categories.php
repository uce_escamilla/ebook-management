<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/menu.js"></script>
    <link type="text/css" rel="stylesheet" href="css/style.css">

    <script src="js/jquery.star.rating.js"></script>
</head>

<body>
    <nav>
        <!-- Nav -->
        <div class="nav-wrapper col s9">

            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <a href="#" class="brand-logo left hide-on-med-and-down hide-on-small-only">Logo</a>
            <a href="#" class="brand-logo hide-on-large-only	">Logo</a>
            <ul class="right hide-on-med-and-down">
                <div class="left">
                    <li><a href="#">Inicio</a></li>
                    <li><a href="#">Todos los libros</a></li>
                    <li><a href="#">Últimos añadidos</a></li>
                    <li><a href="#">Más Leidos</a></li>
                    <li><a href="#">Mejor votados</a></li>
                    <li><a href="#">Categorías</a></li>
                    <li>
                        <!-- Buscador -->
                        <div class="nav-wrapper">
                            <form>
                                <div class="input-field">
                                    <input id="search" type="search" required>
                                    <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                                    <i class="material-icons">close</i>
                                </div>
                            </form>
                        </div>
                    </li>

                </div>
            </ul>
            <ul class="side-nav" id="mobile-demo">
                <li><a href="#">Inicio</a></li>
                <li><a href="#">Todos los libros</a></li>
                <li><a href="#">Últimos añadidos</a></li>
                <li><a href="#">Más Leidos</a></li>
                <li><a href="#">Mejor votados</a></li>
                <li><a href="#">Categorías</a></li>
            </ul>
        </div>
    </nav>
    <!-- Cierre Nav -->
    <div class="row"></div>
    <div class="row">
        <div class="row">
            <a class="waves-effect col s3 waves-light btn">Categoría</a>
            <a class="waves-effect col s3 waves-light btn">Categoría</a>
            <a class="waves-effect col s3 waves-light btn">Categoría</a>
            <a class="waves-effect col s3 waves-light btn">Categoría</a>
        </div>
        <div class="row">
            <a class="waves-effect col s3 waves-light btn">Categoría</a>
            <a class="waves-effect col s3 waves-light btn">Categoría</a>
            <a class="waves-effect col s3 waves-light btn">Categoría</a>
            <a class="waves-effect col s3 waves-light btn">Categoría</a>
        </div>
    </div>

    <div class="row"></div>
    <div class="row">
        <div class="libro col s12 l3">
            <div class="row"></div>
            <div class="slider">
                <ul class="slides">
                    <li>
                        <img src="https://lorempixel.com/580/250/nature/4">
                        <!-- random image -->
                        <div class="caption center-align">
                            <h3>Culpa adipisicing cillum cupidatat.</h3>
                            <h5 class="light grey-text text-lighten-3">Quis ad in et exercitation.</h5>
                            <div class="row center-align">
                                <div class="rating left"></div>
                                <div class="chip right">12</div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row"></div>
        </div>
        <div class="libro col s12 l3">
            <div class="row"></div>
            <div class="slider">
                <ul class="slides">
                    <li>
                        <img src="https://lorempixel.com/580/250/nature/4">
                        <!-- random image -->
                        <div class="caption center-align">
                            <h3>Culpa adipisicing cillum cupidatat.</h3>
                            <h5 class="light grey-text text-lighten-3">Quis ad in et exercitation.</h5>
                            <div class="row center-align">
                                <div class="rating left"></div>
                                <div class="chip right">12</div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row"></div>
        </div>
        <div class="libro col s12 l3">
            <div class="row"></div>
            <div class="slider">
                <ul class="slides">
                    <li>
                        <img src="https://lorempixel.com/580/250/nature/4">
                        <!-- random image -->
                        <div class="caption center-align">
                            <h3>Culpa adipisicing cillum cupidatat.</h3>
                            <h5 class="light grey-text text-lighten-3">Quis ad in et exercitation.</h5>
                            <div class="row center-align">
                                <div class="rating left"></div>
                                <div class="chip right">12</div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row"></div>
        </div>
        <div class="libro col s12 l3">
            <div class="row"></div>
            <div class="slider">
                <ul class="slides">
                    <li>
                        <img src="https://lorempixel.com/580/250/nature/4">
                        <!-- random image -->
                        <div class="caption center-align">
                            <h3>Culpa adipisicing cillum cupidatat.</h3>
                            <h5 class="light grey-text text-lighten-3">Quis ad in et exercitation.</h5>
                            <div class="row center-align">
                                <div class="rating left"></div>
                                <div class="chip right">12</div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="row"></div>
        </div>
    </div>
    <div class="row"></div>
</body>
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            © 2014 Copyright Text
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
        </div>
    </div>
</footer>
</html>