# Personal Ebook Management (PROYECTO EN CONSTRUCCIÓN)

Administrador de libros PDF/Epub en página web.

## Instalación para pruebas
Cambiar la configuración de la base de datos en ```application/config/database.php``` para su respectivo entorno.

Cambiar ```base_url``` en ```application/config/config.php``` para su respectivo entorno.

## Requerimientos

* PHP 5.2+
* Codeigniter 3
* ModRewrite habilitado en Apache

## Tecnologías
* Codeigniter - Framework Backend
* Materialize - Framework Frontend
* PDF JS

## Análisis del proyecto

### Archivos en directorio principal
```*.php``` Prototipos en HTML para su visualización renderizada en el navegador.

### Doc/
Archivos de requerimientos, Reglas de negocio, diccionario de datos.

### Scheme/
Procedimientos para consultas de pruebas.

```final-scheme.mwb``` Proyecto del diagrama de la Base de Datos.

```proc_*``` Procedimientos para consultas.

```diagrama_img.png``` Diagrama de clases de base de datos.

```db_book_mng.sql``` Query para construcción de la Base de Datos.

### Mockup/
Prototipos del diseño del proyecto en ```index.html```. 

Archivo del proyeto en ```WebEbook.epgz```

