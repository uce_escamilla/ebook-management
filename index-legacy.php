<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import jQuery before materialize.js-->
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/menu.js"></script>
    <link type="text/css" rel="stylesheet" href="css/style.css">

    <script src="js/jquery.star.rating.js"></script>
</head>

<body>
    <nav>
        <div class="nav-wrapper col s9">

            <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
            <a href="#" class="brand-logo left hide-on-med-and-down hide-on-small-only">Logo</a>
            <a href="#" class="brand-logo hide-on-large-only">Logo</a>
            <ul class="right hide-on-med-and-down">
                <div class="left">
                    <li><a href="user_edit.php" class="transition">Inicio</a></li>
                    <li><a href="#">Todos los libros</a></li>
                    <li><a href="#">Últimos añadidos</a></li>
                    <li><a href="#">Más Leidos</a></li>
                    <li><a href="#">Mejor votados</a></li>
                    <li><a href="#">Categorías</a></li>
                    <li>
                        <div class="nav-wrapper">
                            <form>
                                <div class="input-field">
                                    <input id="search" type="search" required>
                                    <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                                    <i class="material-icons">close</i>
                                </div>
                            </form>
                        </div>
                    </li>

                </div>
            </ul>
            <ul class="side-nav" id="mobile-demo">
                <li><a href="#">Inicio</a></li>
                <li><a href="#">Todos los libros</a></li>
                <li><a href="#">Últimos añadidos</a></li>
                <li><a href="#">Más Leidos</a></li>
                <li><a href="#">Mejor votados</a></li>
                <li><a href="#">Categorías</a></li>
            </ul>
        </div>
    </nav>

    <!-- Botones -->
    <div class="center-align">
        <h3>Opciones</h3>
    </div>
    <div class="container">
        <div class="divider"></div>
    
        <div class="row">
            <a class="waves-effect waves-light btn-large tooltipped col s4 grey lighten-1 z-depth-1" data-position="bottom" data-delay="50"
                data-tooltip="Añadir un libro">
            <i class="material-icons">add_circle_outline</i></a>
            <a class="waves-effect waves-light btn-large tooltipped col s4 yellow lighten-4 z-depth-1" data-position="bottom" data-delay="50"
                data-tooltip="Sorpréndeme">
            <i class="material-icons">star_border</i></a>
            <a class="waves-effect waves-light btn-large tooltipped col s4 yellow lighten-4 z-depth-1" data-position="bottom" data-delay="50"
                data-tooltip="Estadísticas">
            <i class="material-icons">trending_up</i></a>
        </div>
        <div class="center-align">
            <h3>Continuar Leyendo</h3>
        </div>
        <div class="divider"></div>
    
        <div class="carousel">
            <a class="carousel-item" href="#one!"><img src="img/book1.png"></a>
            <a class="carousel-item" href="#two!"><img src="img/book1.png"></a>
            <a class="carousel-item" href="#three!"><img src="img/book1.png"></a>
            <a class="carousel-item" href="#four!"><img src="img/book1.png"></a>
            <a class="carousel-item" href="#five!"><img src="img/book1.png"></a>
        </div>
    
        <div class="row"></div>
    </div>


</body>
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            © 2014 Copyright Text
            <a class="grey-text text-lighten-4 right" href="#!">More Links</a>
        </div>
    </div>
</footer>
</html>